
/**
 * Gallery
 * @param {Array} list data
 * @param {number} width waper width
 * @param {number} h
 * @param {number} gap padding
 */
function Gallery_H(list, width, h, gap) {
  var L = JSON.parse(JSON.stringify(list)),
    cacheRow = [],
    cacheRowWidth = 0,
    cacheNextWidth = 0,
    newList = [];

  // 统一图片高度
  L = L.map(function (v) {
    v._height = h;
    v._width = h * v.width / v.height;
    return v;
  });

  // core
  L.forEach(function (v, i, arr) {
    cacheRow.push(v);
    cacheRowWidth += v._width;
    cacheNextWidth = cacheRowWidth + (arr[i+1] ? arr[i+1]._width: 0);

    if (judgment()) {
      var r = cacheRowWidth / width;

      if (gap) {
        r = cacheRowWidth/(width - gap*cacheRow.length);
      }

      newList = newList.concat(cacheRow.map(function (item) {
        item._width = item._width / r;
        item._height = item._height / r;

        if (gap) {
          item._width = floor(item._width + gap);
          item._height = floor(item._height + gap);
          item._padding = gap;
        }

        return item;
      }));

      cacheRow = [];
      cacheRowWidth = 0;
    }

    if (i === (L.length - 1)) {
      newList = newList.concat(cacheRow.map(function (item) {
        if (gap) {
          item._width = floor(item._width + gap);
          item._height = floor(item._height + gap);
          item._padding = gap;
        }

        return item;
      }));
    }
  });

  // 换行规则
  function judgment() {
    if (width/cacheRowWidth*h < h*1.1) {
      return true;
    }

    if (width/cacheNextWidth*h < h*0.8 && width/cacheRowWidth*h < h*1.5) {
      return true;
    }

    return false;
  }

  function floor (num) {
    return Math.floor(num * 100) / 100;
  }

  return newList;
}
